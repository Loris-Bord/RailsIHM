package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.ICouleurWagon;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import java.util.Objects;

/**
 * Cette classe représente la vue d'une carte Wagon.
 *
 * On y définit le listener à exécuter lorsque cette carte a été choisie par l'utilisateur
 */
public class VueCarteWagon extends VBox {

    private ICouleurWagon couleurWagon;
    private StackPane carte;
    private Rectangle imgCarte;
    private Label nombre;
    private IntegerProperty compteur = new SimpleIntegerProperty();
    private boolean cartesDuJoueurs;
    private Rectangle iconeSurvol;


    public VueCarteWagon(ICouleurWagon couleurWagon, Double rotation, boolean cartesDuJoueurs) {

        if (rotation != null){
            imgCarte = new Rectangle(180,120);
            iconeSurvol = new Rectangle(50,50);
            imgCarte.setFill(new ImagePattern(new Image("file:src/main/resources/images/cartesWagons/carte-wagon-"+ couleurWagon.toString().toUpperCase() +".png")));
            iconeSurvol.setFill(new ImagePattern(new Image("file:src/main/resources/images/main.png")));
        } else {
            imgCarte = new Rectangle(120,180);
            iconeSurvol = new Rectangle(50,50);
            ImageView fond = new ImageView("file:src/main/resources/images/cartesWagons/carte-wagon-"+ couleurWagon.toString().toUpperCase() +".png");
            fond.setRotate(90.0);
            Image fondFinal = fond.snapshot(new SnapshotParameters(),null);
            iconeSurvol.setFill(new ImagePattern(new Image("file:src/main/resources/images/main.png")));
            imgCarte.setFill(new ImagePattern(fondFinal));
        }

        carte = new StackPane();

        carte.getChildren().add(imgCarte);

        this.cartesDuJoueurs = cartesDuJoueurs;
        nombre = new Label("1");

        setAlignment(Pos.CENTER);
        this.couleurWagon = couleurWagon;
        setSpacing(0);
        nombre.setFont(new Font("Arial", 20));
        nombre.setTextFill(Color.WHITE);
        nombre.setStyle("-fx-font-weight: bold");


        compteur.addListener((observableValue, number, t1) -> nombre.setText("" + t1));
        getChildren().add(carte);
        if (this.cartesDuJoueurs){
            getChildren().add(nombre);
        }
        this.setAlignment(Pos.CENTER);
    }

    public void creerBindings(){
        this.setOnMouseClicked(actionEvent ->{
            ((VueDuJeu) getScene().getRoot()).getJeu().uneCarteWagonAEteChoisie(this.couleurWagon);
        });

        this.setOnMouseEntered(actionEvent -> {
            carte.getChildren().add(iconeSurvol);
        });

        this.setOnMouseExited(actionEvent -> {
            carte.getChildren().remove(iconeSurvol);
        });
    }


    public ICouleurWagon getCouleurWagon() {
        return couleurWagon;
    }

    public Label getLabel() {
        return nombre;
    }


    public void incrementer(int nbFois){
        for (int i = 0; i < nbFois; i++) {
            compteur.setValue(compteur.getValue()+1);
        }
    }

    public void decrementer(int nbFois){
        for (int i = 0; i < nbFois; i++) {
            compteur.setValue(compteur.getValue()-1);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VueCarteWagon that = (VueCarteWagon) o;
        if (this.cartesDuJoueurs) return ((VueCarteWagon) o).couleurWagon.toString().equals(this.couleurWagon.toString());
        else return ((VueCarteWagon) o).couleurWagon.toString().equals(this.couleurWagon.toString()) && ((VueCarteWagon) o).carte == this.carte;
    }

    @Override
    public int hashCode() {
        return Objects.hash(couleurWagon.toString());
    }
}
