package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.IJoueur;
import fr.umontpellier.iut.IRoute;
import fr.umontpellier.iut.IVille;
import fr.umontpellier.iut.rails.Joueur;
import fr.umontpellier.iut.rails.Route;
import fr.umontpellier.iut.rails.Ville;
import javafx.application.Platform;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import javafx.beans.value.ChangeListener;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.util.HashMap;

/**
 * Cette classe présente les routes et les villes sur le plateau.
 *
 * On y définit le listener à exécuter lorsque qu'un élément du plateau a été choisi par l'utilisateur
 * ainsi que les bindings qui mettront ?à jour le plateau après la prise d'une route ou d'une ville par un joueur
 */
public class VuePlateau extends Pane {

    @FXML
    private Group villes;

    @FXML
    private Group routes;

    private ObjectBinding villeDuJoueur;

    private VBox cartesVisibles;

    private HBox cartesJoueurCourant;

    private Node source;

    private ObjectProperty<String> instruction;

    private ObjectProperty<Boolean> cliquer;

    private HBox pioches;

    private VBox destinations;

    private VueDuJeu vueDuJeu;

    private ObjectProperty<IJoueur> joueurcourantProperty;

    private HashMap<IJoueur.Couleur, String> couleurWagonStringHashMap = new HashMap<>(){{
        put(IJoueur.Couleur.BLEU, "BLUE");
        put(IJoueur.Couleur.JAUNE, "YELLOW");
        put(IJoueur.Couleur.VERT, "GREEN");
        put(IJoueur.Couleur.ROUGE, "RED");
        put(IJoueur.Couleur.ROSE, "PINK");
    }};

    public VuePlateau(VBox cartesVisibles, HBox cartesJoueurCourant, ObjectProperty<Boolean> cliquer, HBox pioches, VBox destinations, VueDuJeu jeu) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/plateau.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.cartesVisibles = cartesVisibles;
        this.cartesJoueurCourant = cartesJoueurCourant;
        instruction = new SimpleObjectProperty<>();
        this.cliquer = cliquer;
        this.pioches = pioches;
        this.destinations = destinations;
        this.vueDuJeu = jeu;
        joueurcourantProperty = new SimpleObjectProperty<>();
    }

    @FXML
    public void choixRouteOuVille(Event event) {
        cliquer.setValue(true);
        source = (Node) event.getSource();
        vueDuJeu.timer.stop();

        this.setOpacity(0.5);
        vueDuJeu.setDisable(true);

        VBox pane = new VBox();
        CustomButton buttonOui = new CustomButton("Oui",200);
        CustomButton buttonNon = new CustomButton("Non",200);
        javafx.scene.control.Label label = new javafx.scene.control.Label("Confirmer l'action ?");

        HBox boite = new HBox();
        boite.getChildren().addAll(buttonOui,buttonNon);
        boite.setAlignment(Pos.CENTER);
        boite.setSpacing(15);
        label.setFont(new Font("Cambria", 20));
        label.setTextFill(Color.BLACK);

        pane.getChildren().addAll(label,boite);
        pane.setAlignment(Pos.CENTER);

        HBox.setHgrow(buttonOui, Priority.ALWAYS);
        HBox.setHgrow(buttonNon, Priority.ALWAYS);

        Scene deuxiemeScene = new Scene(pane, 500, 180);

        Stage pause = new Stage();

        pause.setResizable(false);
        pause.setTitle("Confirmation");
        pause.setScene(deuxiemeScene);

        buttonOui.setOnAction(actionEvent -> {
            this.setOpacity(1);
            vueDuJeu.setDisable(false);
            ((VueDuJeu) getScene().getRoot()).getJeu().uneVilleOuUneRouteAEteChoisie(source.getId());
            vueDuJeu.timer.play();
            pause.close();
        });

        buttonNon.setOnAction(actionEvent -> {
            this.setOpacity(1);
            vueDuJeu.setDisable(false);
            vueDuJeu.timer.play();
            pause.close();
        });

        ChangeListener<Boolean> fenetreMontre = (observableValue, aBoolean, t1) -> {
            if(!observableValue.getValue()){
                this.setOpacity(1);
                vueDuJeu.timer.play();
                vueDuJeu.setDisable(false);
            }
        };
        pause.showingProperty().addListener(fenetreMontre);

        pause.show();
    }

    public void creerBindings(){

        Platform.runLater(() -> {
            instruction.bind(((VueDuJeu) getScene().getRoot()).getJeu().instructionProperty());
            joueurcourantProperty.bind(((VueDuJeu) getScene().getRoot()).getJeu().joueurCourantProperty());
        });

        ChangeListener<Joueur> changeListenerRoutes = (observable, old, nouveau) -> {
            Platform.runLater(() -> {
                for (Node route : ((Group) source).getChildren()){
                    ((Rectangle) route).setFill(Color.valueOf(couleurWagonStringHashMap.get(observable.getValue().getCouleur())));
                }
                cliquer.setValue(false);
            });
        };

        Platform.runLater(() -> {
            for (Node group : routes.getChildren()){
                trouveRouteString(group.getId()).proprietaireProperty().addListener(changeListenerRoutes);
            }
        });

        ChangeListener<Joueur> changeListener = (observableValue, old, nouveau) -> {
            Platform.runLater(() -> {
                ((Circle) source).setFill(Color.valueOf(couleurWagonStringHashMap.get(observableValue.getValue().getCouleur())));
            });
        };

        Platform.runLater(() -> {
            for (Node circle : villes.getChildren()) {
                trouveVilleString(((Circle) circle).getId()).proprietaireProperty().addListener(changeListener);
            }
        });

        ChangeListener<IJoueur> joueurCourant = (observable, s, t1) -> {
            Platform.runLater(() -> {
                //setBorder(new Border(new BorderStroke(Color.valueOf(couleurWagonStringHashMap.get(joueurcourantProperty.getValue().getCouleur())),BorderStrokeStyle.SOLID,CornerRadii.EMPTY,new BorderWidths(50.0))));
            });
        };

        joueurcourantProperty.addListener(joueurCourant);



        ChangeListener<String> changeListenerInstruction = ((observableValue, s, t1) -> {
            Platform.runLater(() -> {
                if (cliquer.getValue()){
                    cartesVisibles.setOpacity(0.75);
                    cartesVisibles.setDisable(true);
                    pioches.setDisable(true);
                    destinations.setDisable(true);
                    pioches.setOpacity(0.75);
                    destinations.setOpacity(0.75);
                    for(Node carte : cartesJoueurCourant.getChildren()){
                        ((VueCarteWagon) carte).creerBindings();
                    }
                    cliquer.setValue(false);
                }
            });
        });

        instruction.addListener(changeListenerInstruction);

    }

    public IVille trouveVilleString(String ville){
        for(Object ville1 : ((VueDuJeu) getScene().getRoot()).getJeu().getVilles()){
            if(((Ville) ville1).getNom().equals(ville)) return (Ville) ville1;
        }
        return null;
    }

    public IRoute trouveRouteString(String route){
        for(Object route1 : ((VueDuJeu) getScene().getRoot()).getJeu().getRoutes()){
            if(((Route) route1).getNom().equals(route)) return (Route) route1;
        }
        return null;
    }



}