package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.IJoueur;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

import java.util.HashMap;

/**
 * Cette classe présente les éléments appartenant au joueur courant.
 *
 * On y définit les bindings sur le joueur courant, ainsi que le listener à exécuter lorsque ce joueur change
 */
public class VueJoueurCourant extends HBox {

    private Label nomJoueur;
    private Label scoreDuJoueur;
    private Label nbGareJoueurCourant;
    private Label prenomJoueur;
    private Label scoreJoueur;
    private Rectangle avatarJoueur;
    private ObjectProperty<ImagePattern> imageProperty;
    private ObjectProperty<ImagePattern> imgGare;
    private ObjectProperty<Integer> scoreDuJoueurProperty;
    private ObjectProperty<Integer> nombreDeGareProperty;
    private ObjectProperty<String> nomJC;
    private ObjectProperty<Integer> wagonsDuJoueurProperty;
    private VBox donnees;
    private HBox gares;
    private HBox nJoueur;
    private VBox nbPoints;
    private Rectangle Igares;
    private StackPane icone;
    private Circle contour;
    private Rectangle barre;
    private StackPane paneGare;
    private Circle gareCircle;
    private HBox cartesWagonLogoBox;
    private StackPane paneWagons;
    private Circle wagonsCircle;
    private Rectangle Iwagons;
    private Label nbWagonsJoueurCourant;

    private HashMap<IJoueur.Couleur, String> couleurWagonStringHashMap = new HashMap<>(){{
        put(IJoueur.Couleur.BLEU, "BLUE");
        put(IJoueur.Couleur.JAUNE, "YELLOW");
        put(IJoueur.Couleur.VERT, "GREEN");
        put(IJoueur.Couleur.ROUGE, "RED");
        put(IJoueur.Couleur.ROSE, "PINK");
    }};

    public VueJoueurCourant(){
        barre = new Rectangle(200,5);
        gares = new HBox();
        cartesWagonLogoBox = new HBox();
        nJoueur = new HBox();
        nbPoints = new VBox();
        donnees = new VBox();
        prenomJoueur = new Label("Nom : ");
        scoreJoueur = new Label("Score : ");
        icone = new StackPane();
        contour = new Circle(100);
        setAlignment(Pos.CENTER);
        scoreDuJoueur = new Label();
        nomJoueur = new Label();
        nbWagonsJoueurCourant = new Label();
        Iwagons = new Rectangle(32,32);
        nbGareJoueurCourant = new Label();
        avatarJoueur = new Rectangle(110,110);
        imageProperty = new SimpleObjectProperty<>();
        imgGare = new SimpleObjectProperty<>();
        nomJC = new SimpleObjectProperty<>();
        scoreDuJoueurProperty = new SimpleObjectProperty<>();
        nombreDeGareProperty = new SimpleObjectProperty<>();
        wagonsDuJoueurProperty = new SimpleObjectProperty<>();
        Igares = new Rectangle(32,32);
        donnees.setAlignment(Pos.CENTER);
        paneGare = new StackPane();
        paneWagons = new StackPane();

        wagonsCircle = new Circle(28);
        wagonsCircle.setFill(Color.WHITE);
        wagonsCircle.setStrokeWidth(3);

        gareCircle = new Circle(28);
        gareCircle.setFill(Color.WHITE);
        gareCircle.setStrokeWidth(3);

        cartesWagonLogoBox.getChildren().addAll(Iwagons, nbWagonsJoueurCourant);
        donnees.getChildren().addAll(nJoueur,paneGare, paneWagons);
        nJoueur.getChildren().addAll(prenomJoueur,nomJoueur);
        nbPoints.getChildren().addAll(barre,scoreDuJoueur);
        gares.getChildren().addAll(Igares,nbGareJoueurCourant);
        paneGare.getChildren().addAll(gareCircle,gares);
        paneWagons.getChildren().addAll(wagonsCircle, cartesWagonLogoBox);

        nJoueur.setSpacing(5);
        nbPoints.setSpacing(5);
        gares.setSpacing(5);
        paneGare.setAlignment(Pos.CENTER);
        gares.setAlignment(Pos.CENTER);
        paneWagons.setAlignment(Pos.CENTER);
        cartesWagonLogoBox.setAlignment(Pos.CENTER);

        getChildren().add(donnees);
        getChildren().add(icone);

        icone.getChildren().addAll(contour, avatarJoueur, nbPoints);

        icone.setAlignment(Pos.TOP_CENTER);
        contour.setStrokeWidth(3.5);
        contour.setFill(null);
        nbPoints.setAlignment(Pos.BOTTOM_CENTER);
        icone.setPadding(new Insets(35,0,0,0));
        nbPoints.setPadding(new Insets(0,0,40,0));


        nbGareJoueurCourant.setFont(new Font("Cambria",20));
        nbWagonsJoueurCourant.setFont(new Font("Cambria",20));
        nomJoueur.setFont(new Font("Cambria",20));
        scoreDuJoueur.setFont(new Font("Cambria",41));
        prenomJoueur.setFont(new Font("Cambria", 20));
        scoreJoueur.setFont(new Font("Cambria", 20));
        nbGareJoueurCourant.setTextFill(Color.BLACK);
        nbWagonsJoueurCourant.setTextFill(Color.BLACK);
        nomJoueur.setTextFill(Color.WHITE);
        scoreDuJoueur.setTextFill(Color.WHITE);
        prenomJoueur.setTextFill(Color.WHITE);
        scoreJoueur.setTextFill(Color.WHITE);
        nbGareJoueurCourant.setStyle("-fx-font-weight: bold");
        nbWagonsJoueurCourant.setStyle("-fx-font-weight: bold");
        nomJoueur.setStyle("-fx-font-weight: bold");
        scoreDuJoueur.setStyle("-fx-font-weight: bold");
        prenomJoueur.setStyle("-fx-font-weight: bold");
        scoreJoueur.setStyle("-fx-font-weight: bold");
    }

    public void creerBindings(){
        ChangeListener<IJoueur> iJoueurChangeListener = (observableValue, iJoueur, t1) -> {
            Platform.runLater(() -> {
                imageProperty.setValue(new ImagePattern(new Image("file:src/main/resources/images/ressources_images_images_avatar-" + observableValue.getValue().getCouleur() + ".png")));
                imgGare.setValue(new ImagePattern(new Image("file:src/main/resources/images/gares/gare-" + observableValue.getValue().getCouleur() + ".png")));
                contour.setStroke(Color.valueOf(couleurWagonStringHashMap.get(observableValue.getValue().getCouleur())));
                nomJC.setValue(observableValue.getValue().getNom());
                scoreDuJoueurProperty.setValue(observableValue.getValue().getScore());
                nombreDeGareProperty.setValue(observableValue.getValue().getNbGares());
                barre.setFill(Color.valueOf(couleurWagonStringHashMap.get(observableValue.getValue().getCouleur())));
                wagonsCircle.setStroke(Color.valueOf(couleurWagonStringHashMap.get(observableValue.getValue().getCouleur())));
                gareCircle.setStroke(Color.valueOf(couleurWagonStringHashMap.get(observableValue.getValue().getCouleur())));
                Iwagons.setFill(new ImagePattern(new Image("file:src/main/resources/images/wagons/image-wagon-" + observableValue.getValue().getCouleur().name().toUpperCase() + ".png")));
                wagonsDuJoueurProperty.setValue(observableValue.getValue().getNbWagons());
            });
        };
        Platform.runLater(() -> {
            ObjectProperty<IJoueur> joueurCourant = ((VueDuJeu) getScene().getRoot()).getJeu().joueurCourantProperty();
            joueurCourant.addListener(iJoueurChangeListener);
            if (nomJC.getValue() == null) nomJC.setValue(joueurCourant.getValue().getNom());
            if (scoreDuJoueurProperty.getValue() == null) scoreDuJoueurProperty.setValue(joueurCourant.getValue().getScore());
            if (nombreDeGareProperty.getValue() == null) nombreDeGareProperty.setValue(joueurCourant.getValue().getNbGares());
            nbGareJoueurCourant.textProperty().bind(Bindings.concat(nombreDeGareProperty));
            scoreDuJoueur.textProperty().bind(Bindings.concat(scoreDuJoueurProperty));
            nomJoueur.textProperty().bind(nomJC);
            imageProperty.setValue(new ImagePattern(new Image("file:src/main/resources/images/ressources_images_images_avatar-" + joueurCourant.getValue().getCouleur() + ".png")));
            imgGare.setValue(new ImagePattern(new Image("file:src/main/resources/images/gares/gare-" + joueurCourant.getValue().getCouleur() + ".png")));
            wagonsDuJoueurProperty.setValue(joueurCourant.getValue().getNbWagons());
            contour.setStroke(Color.valueOf(couleurWagonStringHashMap.get(joueurCourant.getValue().getCouleur())));
            barre.setFill(Color.valueOf(couleurWagonStringHashMap.get(joueurCourant.getValue().getCouleur())));
            gareCircle.setStroke(Color.valueOf(couleurWagonStringHashMap.get(joueurCourant.getValue().getCouleur())));
            wagonsCircle.setStroke(Color.valueOf(couleurWagonStringHashMap.get(joueurCourant.getValue().getCouleur())));
            Iwagons.setFill(new ImagePattern(new Image("file:src/main/resources/images/wagons/image-wagon-" + joueurCourant.getValue().getCouleur().name().toUpperCase() + ".png")));
            avatarJoueur.fillProperty().bind(imageProperty);
            Igares.fillProperty().bind(imgGare);
            nbWagonsJoueurCourant.textProperty().bind(Bindings.concat(wagonsDuJoueurProperty));
        });
    }

    public ObjectProperty<ImagePattern> getImageProperty(){
        return this.imageProperty;
    }

    public void setImageProperty(ImagePattern imageProperty){
        this.imageProperty.setValue(imageProperty);
    }


}