package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.IJeu;
import fr.umontpellier.iut.IJoueur;
import fr.umontpellier.iut.rails.Joueur;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Cette classe présente les éléments des joueurs autres que le joueur courant,
 * en cachant ceux que le joueur courant n'a pas à connaitre.
 *
 * On y définit les bindings sur le joueur courant, ainsi que le listener à exécuter lorsque ce joueur change
 */
public class VueAutresJoueurs extends VBox {

    private HashMap<IJoueur.Couleur, String> couleurWagonStringHashMap = new HashMap<>(){{
        put(IJoueur.Couleur.BLEU, "BLUE");
        put(IJoueur.Couleur.JAUNE, "YELLOW");
        put(IJoueur.Couleur.VERT, "GREEN");
        put(IJoueur.Couleur.ROUGE, "RED");
        put(IJoueur.Couleur.ROSE, "PINK");
    }};

    private IJeu jeu;
    private ArrayList<HBox> listeJoueurs;
    private Rectangle joueurFill;
    private ObjectProperty<ImagePattern> imageProperty;
    private Label nomJoueur;
    private Label scoreJoueur;
    private VBox informations;
    private StackPane statsJoueursPane;
    private Label cartesWagonJoueur;
    private Circle nbWagonJoueurCircle;
    private VBox logoEtNbWagonsBox;
    private ImagePattern wagonJoueurImage;
    private Rectangle rectangleWagonDuJoueur;
    private HBox boxNomJoueurScore;
    private StackPane scoreJoueurPane;
    private Circle circleScore;
    private HBox boxNbWagonsJoueur;

    public VueAutresJoueurs(IJeu jeu){
        this.jeu = jeu;
        listeJoueurs = new ArrayList<>();
        listeJoueurs.add(new HBox());
        listeJoueurs.add(new HBox());
        listeJoueurs.add(new HBox());
        listeJoueurs.add(new HBox());

        for (HBox listeJoueur : listeJoueurs) {
            getChildren().add(listeJoueur);
        }

    }

    public void creerBindings(){
        ChangeListener<IJoueur> iJoueurChangeListener = (observableValue, iJoueur, t1) -> {
            Platform.runLater(() -> {
                int i = 0;
                for(Joueur joueur : jeu.getJoueurs()) {
                    if (!joueur.getNom().equals(jeu.joueurCourantProperty().getValue().getNom())) {
                        listeJoueurs.get(i).getChildren().clear();
                        listeJoueurs.get(i).setBackground(new Background(new BackgroundFill(Color.valueOf(couleurWagonStringHashMap.get(joueur.getCouleur())), null, null)));
                        joueurFill = new Rectangle(100,100);
                        nomJoueur = new Label(joueur.getNom());
                        cartesWagonJoueur = new Label(String.valueOf(joueur.getNbWagons()));
                        scoreJoueur = new Label(String.valueOf(joueur.getScore()));
                        statsJoueursPane = new StackPane();
                        scoreJoueurPane = new StackPane();
                        boxNomJoueurScore = new HBox();
                        boxNbWagonsJoueur = new HBox();
                        wagonJoueurImage = new ImagePattern(new Image("file:src/main/resources/images/wagons/image-wagon-" + joueur.getCouleur().name().toUpperCase() + ".png"));

                        rectangleWagonDuJoueur = new Rectangle(30,25);
                        rectangleWagonDuJoueur.setFill(wagonJoueurImage);

                        logoEtNbWagonsBox = new VBox();
                        logoEtNbWagonsBox.getChildren().addAll(rectangleWagonDuJoueur,cartesWagonJoueur);
                        logoEtNbWagonsBox.setAlignment(Pos.CENTER);
                        logoEtNbWagonsBox.setSpacing(0);

                        circleScore = new Circle(19);
                        circleScore.setFill(Color.WHITE);
                        circleScore.setStroke(Color.BLACK);

                        nbWagonJoueurCircle = new Circle(19);
                        nbWagonJoueurCircle.setFill(Color.WHITE);
                        nbWagonJoueurCircle.setStroke(Color.BLACK);

                        scoreJoueurPane.getChildren().addAll(circleScore, scoreJoueur);
                        statsJoueursPane.getChildren().addAll(nbWagonJoueurCircle,logoEtNbWagonsBox);
                        boxNbWagonsJoueur.getChildren().addAll(statsJoueursPane);
                        informations = new VBox();
                        boxNomJoueurScore.getChildren().addAll(nomJoueur, scoreJoueurPane);
                        boxNomJoueurScore.setSpacing(-(nomJoueur.getText().length()/0.1)+90);
                        boxNbWagonsJoueur.setAlignment(Pos.BASELINE_LEFT);

                        imageProperty = new SimpleObjectProperty<>();
                        imageProperty.setValue(new ImagePattern(new Image("file:src/main/resources/images/ressources_images_images_avatar-" + joueur.getCouleur() + ".png")));
                        listeJoueurs.get(i).getChildren().add(joueurFill);
                        informations.getChildren().addAll(boxNomJoueurScore,boxNbWagonsJoueur);
                        informations.setAlignment(Pos.BOTTOM_LEFT);
                        informations.setSpacing(17);
                        listeJoueurs.get(i).getChildren().add(informations);

                        joueurFill.fillProperty().bind(imageProperty);
                        nomJoueur.setTextFill(Color.BLACK);
                        nomJoueur.setStyle("-fx-font-weight: bold");
                        nomJoueur.setFont(new Font("Cambria",20));
                        scoreJoueur.setFont(new Font("Cambria",18));
                        scoreJoueur.setTextFill(Color.BLACK);
                        scoreJoueur.setStyle("-fx-font-weight: bold");
                        cartesWagonJoueur.setStyle("-fx-font-weight: bold");
                        cartesWagonJoueur.setFont(new Font("Cambria",15));
                        i++;
                    }
                }
            });
        };
        this.jeu.joueurCourantProperty().addListener(iJoueurChangeListener);

    }



}
