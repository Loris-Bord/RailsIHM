package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.*;
import fr.umontpellier.iut.rails.CouleurWagon;
import fr.umontpellier.iut.rails.Destination;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Cette classe correspond à la fenêtre principale de l'application.
 *
 * Elle est initialisée avec une référence sur la partie en cours (Jeu).
 *
 * On y définit les bindings sur les éléments internes qui peuvent changer
 * (le joueur courant, les 5 cartes Wagons visibles, les destinations lors de l'étape d'initialisation de la partie, ...)
 * ainsi que les listeners à exécuter lorsque ces éléments changent
 */
public class VueDuJeu extends BorderPane {

    private IJeu jeu;
    private VuePlateau plateau;
    private VBox listeDestinationBox;
    private VueJoueurCourant vueJoueurCourant;
    private VueAutresJoueurs vueAutresJoueurs;
    private VBox listeCartesWagonsVisible;
    private Stage newWindow;
    private HBox destinationsCourante;
    private StackPane destinationsC;
    private VBox console;
    private Rectangle bgConsole;
    private StackPane piocheCarteW;
    private StackPane piocheDestination;
    private Rectangle piocheCartesWagons;
    private Rectangle piocheCartesDestinations;
    private Rectangle iconeSurvol;
    private StackPane plateauFinal;
    private Rectangle borderPlateau;
    private CustomButton button1;
    private Stage pause;

    private StackPane pauseWindow;

    private Stage reglesWindow;


    @FXML
    private HBox top;
    @FXML
    private HBox bot;
    @FXML
    private VBox right;
    @FXML
    private VBox left;

    private Button passer;

    private Button boutonPause;

    @FXML
    private VBox destination;

    @FXML
    private HBox pioches;

    @FXML
    private HBox cartesJoueurCourant;

    private ObjectProperty<Boolean> cliquer;

    private Label time;

    @FXML
    private StackPane timeBg;

    private ObjectProperty<Duration> timeleft;

    public Timeline timer;

    private Rectangle menuPause;

    private Label textPiocheWagons;

    private Label textPiocheDestination;


    private HashMap<IJoueur.Couleur, String> couleurWagonStringHashMap = new HashMap<>(){{
        put(IJoueur.Couleur.BLEU, "BLUE");
        put(IJoueur.Couleur.JAUNE, "YELLOW");
        put(IJoueur.Couleur.VERT, "GREEN");
        put(IJoueur.Couleur.ROUGE, "RED");
        put(IJoueur.Couleur.ROSE, "PINK");
    }};


    public VueDuJeu(IJeu jeu) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/jeu.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.console = new VBox();

        pause = new Stage();
        pauseWindow = new StackPane();
        reglesWindow = new Stage();

        textPiocheWagons = new Label("Pioche Wagons");
        textPiocheDestination = new Label("Pioche Destinations");

        textPiocheWagons.setTextFill(Color.WHITE);
        textPiocheDestination.setTextFill(Color.WHITE);

        textPiocheWagons.setFont(new Font("Cambria", 16));
        textPiocheDestination.setFont(new Font("Cambria", 16));

        textPiocheWagons.setStyle("-fx-font-weight: bold");
        textPiocheDestination.setStyle("-fx-font-weight: bold");

        this.setBackground(new Background(new BackgroundImage(new Image("file:src/main/resources/images/fond.jpg"),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO,true,true,true,true))));

        destinationsC = new StackPane();
        time = new Label();

        menuPause = new Rectangle(450,450);
        menuPause.setFill(new ImagePattern(new Image("file:src/main/resources/images/pause-menu.png")));

        bgConsole = new Rectangle(150,400);

        this.destinationsCourante = new HBox();
        this.vueJoueurCourant = new VueJoueurCourant();
        this.jeu = jeu;
        this.listeCartesWagonsVisible = new VBox();
        this.listeDestinationBox = new VBox();
        listeCartesWagonsVisible.setAlignment(Pos.CENTER_LEFT);
        this.vueAutresJoueurs = new VueAutresJoueurs(jeu);
        this.right.getChildren().add(listeCartesWagonsVisible);
        this.left.getChildren().add(listeDestinationBox);
        this.left.getChildren().add(vueAutresJoueurs);
        this.left.getChildren().add(console);

        console.setPadding(new Insets(0,0,10,0));

        this.time.setTranslateY(-15);


        cliquer = new SimpleObjectProperty<>();

        plateau = new VuePlateau(this.listeCartesWagonsVisible,cartesJoueurCourant, cliquer, pioches, destination,this);
        plateau.creerBindings();

        plateauFinal = new StackPane();
        borderPlateau = new Rectangle(plateau.getPrefWidth()+6, plateau.getPrefHeight()+2);

        borderPlateau.setStrokeWidth(10);
        borderPlateau.setStroke(Color.RED);

        plateauFinal.getChildren().addAll(borderPlateau, plateau);

        this.setCenter(plateauFinal);


        ImageView bgC = new ImageView(new Image("file:src/main/resources/images/console.png"));
        bgC.setRotate(90.0);
        Image bgCFinal = bgC.snapshot(new SnapshotParameters(), null);


        console.setBackground(new Background(new BackgroundImage(bgCFinal,BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(BackgroundSize.AUTO,BackgroundSize.AUTO, true,true,true,true))));
        console.setFillWidth(true);
        console.setPrefWidth(100);
        console.setPrefHeight(200);



        listeCartesWagonsVisible.setFillWidth(true);

        newWindow = new Stage();

        boutonPause = new CustomButton("Pause",220);
        passer = new CustomButton("Passer",220);


        Rectangle dest = new Rectangle(180,120);
        dest.setFill(new ImagePattern(new Image("file:src/main/resources/images/missions/eu-amsterdam-pamplona.png")));
        destinationsC.getChildren().add(dest);
        destination.getChildren().add(destinationsC);

        VueJoueurCourant vueJoueurCourant = new VueJoueurCourant();
        vueJoueurCourant.creerBindings();
        VBox boutonPrincipauxBox = new VBox();
        boutonPrincipauxBox.getChildren().addAll(passer,boutonPause);
        boutonPrincipauxBox.setAlignment(Pos.CENTER);
        boutonPrincipauxBox.setSpacing(10);
        bot.getChildren().add(boutonPrincipauxBox);
        bot.getChildren().add(vueJoueurCourant);
        vueAutresJoueurs.creerBindings();

        piocheCarteW = new StackPane();
        piocheDestination = new StackPane();
        iconeSurvol = new Rectangle(50,50);
        iconeSurvol.setFill(new ImagePattern(new Image("file:src/main/resources/images/main.png")));
        piocheCartesWagons = new Rectangle(180,120);
        piocheCartesDestinations = new Rectangle(180,120);
        piocheCartesWagons.setRotate(90);
        piocheCartesDestinations.setRotate(90);
        piocheCartesWagons.setFill(new ImagePattern(new Image("file:src/main/resources/images/wagons.png")));
        piocheCartesDestinations.setFill(new ImagePattern(new Image("file:src/main/resources/images/destinations.jpg")));

        textPiocheWagons.setPadding(new Insets(195,0,0,0));
        textPiocheDestination.setPadding(new Insets(195,0,0,0));


        piocheCarteW.getChildren().addAll(piocheCartesWagons,textPiocheWagons);
        piocheDestination.getChildren().addAll(piocheCartesDestinations,textPiocheDestination);
        destinationsC.getChildren().add(destinationsCourante);

        pioches.getChildren().addAll(piocheCarteW,piocheDestination);

        Rectangle bg = new Rectangle(280,35);
        bg.setFill(Color.WHITE);
        bg.setArcWidth(10);
        bg.setArcHeight(10);

        timeBg.getChildren().add(bg);

        timer = new Timeline();
        timer.getKeyFrames().add(new KeyFrame(javafx.util.Duration.seconds(1), ae -> updateTimer()));
        timer.setCycleCount(Timeline.INDEFINITE);
        timeleft = new SimpleObjectProperty<>();
        timeleft.set(Duration.ofMinutes(60));
        timer.playFromStart();
        time.setFont(new Font("Cambria",25));
        time.setStyle("-fx-font-weight: bold");
        time.setTextFill(Color.BLACK);
        timeBg.getChildren().add(time);
        time.setTranslateY(3);

        button1 = new CustomButton("Règles",200);
    }

    private void updateTimer(){
        timeleft.setValue(timeleft.getValue().minusSeconds(1));
    }

    private String getTimeStringFromDuration(Duration duration){
        return "Temps : " + duration.toHoursPart() + ":" + duration.toMinutesPart() + ":" + duration.toSecondsPart();
    }

    public IJeu getJeu() {
        return jeu;
    }

    public void creerBindings() {

        button1.setOnMouseClicked(actionEvent -> {
            timer.stop();
            pauseWindow.setDisable(true);
            CustomButton buttonResume = new CustomButton("Revenir",200);
            VBox window = new VBox();
            Label page = new Label("Choissisez une page");
            page.setFont(new Font("Cambria", 20));
            page.setStyle("-fx-font-weight: bold");
            page.setTextFill(Color.BLACK);
            page.setWrapText(true);
            page.setMaxWidth(1000);
            page.setTranslateX(100);
            VBox buttons = new VBox();
            HBox pageConteneur = new HBox();
            HBox.setHgrow(pageConteneur, Priority.ALWAYS);
            buttons.setAlignment(Pos.CENTER);
            buttons.getChildren().add(buttonResume);
            MenuBar menuBar = new MenuBar();
            Menu choix = new Menu("Règles du jeu");
            Menu raccourciAides = new Menu("Raccourci et aides");
            MenuItem raccourci = new MenuItem("Raccourci");
            MenuItem aides = new MenuItem("Aides");
            MenuItem depart = new MenuItem("Tour de jeu");
            MenuItem cartesWagons = new MenuItem("Prendre des cartes wagons");
            MenuItem routes = new MenuItem("Prendre possession des routes");
            MenuItem routesSpe = new MenuItem("Ferries/Tunnels");
            MenuItem cartesDestinations = new MenuItem("Prendre des cartes Destination");
            MenuItem gares = new MenuItem("Bâtir une gare");
            MenuItem points = new MenuItem("Décompte des points");
            choix.getItems().addAll(depart,cartesWagons,routes,routesSpe,cartesDestinations,gares,points);
            raccourciAides.getItems().addAll(raccourci,aides);
            pageConteneur.getChildren().add(page);
            window.getChildren().addAll(menuBar, buttons, pageConteneur);
            menuBar.getMenus().addAll(choix,raccourciAides);
            window.setBackground(new Background(
                    new BackgroundImage(new Image("file:src/main/resources/images/console.png"),
                            BackgroundRepeat.NO_REPEAT,
                            BackgroundRepeat.NO_REPEAT,
                            BackgroundPosition.CENTER,
                            new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, true, true, true,true))));

            Rectangle pane = new Rectangle(1050,600);
            pane.setTranslateX(100);
            pane.setFill(new ImagePattern(new Image("file:src/main/resources/images/aides-regles.png")));

            EventHandler<ActionEvent> action = actionEvent1 -> {
                switch (((MenuItem) actionEvent1.getSource()).getText()){
                    case "Tour de jeu" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Le joueur ayant visité le plus grand nombre de pays européens commence. La partie " +
                                "continue ensuite dans le sens des aiguilles d'une montre. À son tour, le joueur doit " +
                                "faire une, et une seule, des quatre actions suivantes : " +
                                "Prendre des cartes Wagon – Le joueur peut prendre deux cartes Wagon (ou une seule, " +
                                "s’il choisit de prendre une Locomotive, face visible. Se référer à la section Locomotives " +
                                "pour plus de détails) ; " +
                                "Prendre possession d'une Route – Le joueur peut s'emparer d'une route sur le " +
                                "plateau en posant autant de cartes Wagon de la couleur de la route que d'espaces " +
                                "composant cette route. Après avoir défaussé ses cartes, le joueur pose ses wagons " +
                                "sur chaque espace constituant la route, et déplace son marqueur de score d'un " +
                                "nombre de cases correspondant au barème de décompte des points ; " +
                                "Prendre des cartes Destination supplémentaires – Le joueur tire trois cartes " +
                                "Destination du dessus de la pioche. Il doit en conserver au moins une, et remettre " +
                                "celles qu'il ne souhaite pas garder en dessous de la pioche ; " +
                                "Construire une Gare – Le joueur peut bâtir une Gare sur toute ville qui en est jusque " +
                                "là dépourvue. S'il s'agit de sa première Gare, le joueur se défausse d'une carte Wagon, " +
                                "et place sa Gare. S’il s'agit de sa seconde Gare, le joueur doit se défausser de deux " +
                                "cartes de même couleur pour la bâtir. Et s’il s'agit de sa troisième Gare, il doit se " +
                                "défausser de trois cartes de couleur identique. ");
                    }
                    case "Prendre des cartes wagons" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Il existe 8 types de wagons différents en plus des Locomotives. Les couleurs de " +
                                "chaque carte Wagon correspondent aux couleurs des routes présentes sur le plateau " +
                                "connectant des villes – bleu, violet, orange, blanc, vert, jaune, noir et rouge. " +
                                "Lorsqu'un joueur choisit de prendre des cartes Wagon, il peut en piocher jusqu'à deux " +
                                "par tour. Chacune de ces cartes doit être soit prise parmi les cinq faces visibles, soit tirée du dessus de la pioche (tirage à l'aveugle). Lorsqu'une carte " +
                                "face visible est choisie, elle doit être immédiatement remplacée par une nouvelle carte tirée du sommet de la pioche. " +
                                "Si le joueur choisit de prendre une Locomotive face visible parmi les cinq cartes exposées, il ne peut piocher d'autre carte lors de ce tour (pour plus " +
                                "de détails, se référer à la section sur les Locomotives). Si, à un moment quelconque, trois des cinq cartes face visible sont des Locomotives, les cinq " +
                                "cartes sont immédiatement mises de côté et remplacées par cinq nouvelles cartes face visible. " +
                                "Un joueur peut conserver en main autant de cartes qu'il le souhaite durant la partie. Si la pioche est épuisée, la défausse est mélangée pour constituer " +
                                "une nouvelle pioche. Veillez à bien mélanger celle-ci, les cartes étant généralement défaussées par séries de couleur. " +
                                "Dans le cas peu probable où la pioche et la défausse seraient simultanément épuisées car les joueurs ont gardé toutes les cartes en main, le joueur " +
                                "dont c’est le tour ne pourrait choisir de piocher des cartes Wagon ; il se retrouvera alors contraint de choisir une des trois autres options : prendre " +
                                "possession d'une route, piocher de nouvelles cartes Destination, ou bâtir une nouvelle Gare. " +
                                "Locomotives " +
                                "Les Locomotives sont multicolores et, comme des cartes Joker, peuvent remplacer n'importe quelle couleur lors de " +
                                "la prise de possession d'une route. Les Locomotives sont également nécessaires pour emprunter des Ferries " +
                                "(cf. section sur les Ferries). " +
                                "Si une carte Locomotive figure parmi les cinq cartes visibles lors d'une phase de pioche de cartes Wagon, le joueur " +
                                "peut la prendre, mais son tour s’arrête alors immédiatement. La Locomotive compte comme s’il avait pris 2 cartes. Si après avoir pris une carte visible " +
                                "(qui n’est pas une Locomotive), la carte de remplacement tirée est une Locomotive, elle ne peut donc être prise par ce joueur ce tour-ci. " +
                                "Si par contre un joueur a la chance de tirer une Locomotive en aveugle, au sommet de la pioche, les autres joueurs n'en savent rien, et cette carte ne compte " +
                                "que pour une carte piochée. Il peut alors tirer une deuxième carte (en aveugle, ou une carte non Locomotive, parmi les cinq cartes face visible). ");
                    }
                    case "Prendre possession des routes" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Une route est une ligne continue de rectangles colorés ou gris reliant " +
                                "deux villes adjacentes sur le plateau de jeu. Pour prendre possession " +
                                "d'une route, le joueur doit jouer des cartes Wagon de la couleur " +
                                "correspondante, en quantité suffisante pour égaler le nombre d'espaces " +
                                "figurant sur la route convoitée. " +
                                "La plupart des routes nécessitent une série de cartes de couleur " +
                                "spécifique. Des cartes Locomotive peuvent être utilisées pour remplacer " +
                                "des cartes manquantes dans la couleur désirée (exemple 1). " +
                                "Certaines routes – en gris sur le plateau - peuvent être capturées en " +
                                "utilisant n’importe quelle série d’une même couleur (exemple 2). " +
                                "Lorsqu’une route a été capturée, le joueur pose ses wagons en plastique " +
                                "sur chacun des espaces qui constituent la route. Toutes les cartes " +
                                "utilisées pour s’approprier cette route sont défaussées. Le joueur avance " +
                                "alors immédiatement son marqueur de score d'un nombre de cases " +
                                "correspondant au barème indiqué sur le tableau de décompte des points " +
                                "en page 7 de ce livret. " +
                                "Un joueur peut prendre possession de n'importe quelle route sur le " +
                                "plateau de jeu. Il n'est pas obligé de se connecter avec des routes déjà à " +
                                "son actif. " +
                                "Une route doit être capturée dans son intégralité en un tour de jeu. Il est " +
                                "impossible, par exemple, de poser deux trains sur une route longue de " +
                                "trois espaces, puis d'attendre le tour suivant pour jouer une troisième " +
                                "carte et poser son dernier train. " +
                                "Un joueur ne peut jamais capturer plus d'une route par tour de jeu. " +
                                "Routes Doubles " +
                                "Certaines villes sont connectées par des routes doubles. Ces routes sont parallèles de bout en bout, et " +
                                "de longueur identique. Elles connectent obligatoirement les mêmes villes pour les deux routes. " +
                                "Un même joueur ne peut jamais capturer les deux routes d'une même route double durant une partie. " +
                                "Attention : Certaines routes sont en partie parallèles, mais relient différentes villes. Il ne s'agit alors pas " +
                                "de routes doubles, mais simplement de routes (en partie) parallèles. " +
                                "Note importante : dans une configuration à 2 ou 3 joueurs, seule l’une des routes constituant la double connexion peut " +
                                "être utilisée. Un joueur peut prendre possession de l’une des deux routes disponibles, mais dès lors, la route restante " +
                                "demeurera fermée et inaccessible à tous jusqu’en fin de partie. ");
                    }
                    case "Ferries/Tunnels" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Ferries " +
                                "Les Ferries sont des routes spéciales, de couleur grise, reliant " +
                                "deux villes séparées par des eaux. Les lignes de Ferries sont " +
                                "facilement reconnaissables grâce au symbole de Locomotive " +
                                "figurant sur l'un au moins des espaces constituant cette route. " +
                                "Pour prendre possession d'une ligne de Ferries, un joueur doit jouer une " +
                                "carte Locomotive pour chaque symbole de Locomotive figurant sur la " +
                                "route. " +
                                "Tunnels " +
                                "Les Tunnels sont des routes spéciales, reconnaissables à leurs marques et contours " +
                                "spécifiques. " +
                                "Ce qui distingue le tunnel d'une route normale, c'est que le joueur qui l'emprunte n'est jamais " +
                                "sûr de la distance qu'il devra parcourir avant de rejoindre l'autre bout ! " +
                                "Quand il tente de traverser un Tunnel, le joueur joue d'abord le nombre de cartes de la couleur requise, " +
                                "comme à l’accoutumée. Puis il retourne les trois premières cartes du sommet de la pioche de cartes Wagon. " +
                                "Pour chaque carte ainsi révélée se trouvant être de la couleur des cartes utilisées pour traverser le Tunnel, " +
                                "le joueur doit maintenant rajouter une nouvelle carte Wagon de même couleur (ou une Locomotive). C'est " +
                                "uniquement une fois celles-ci rajoutées que le joueur peut prendre possession du Tunnel. " +
                                "Si le joueur ne possède plus assez de cartes de la couleur correspondante (ou ne souhaite pas les jouer " +
                                "maintenant), il reprend toutes ses cartes en main et passe son tour. " +
                                "En fin de tour, les cartes tirées du sommet de la pioche sont défaussées. " +
                                "Attention : N'oubliez pas que les Locomotives sont des cartes multicolores. En conséquence, toute Locomotive figurant parmi les trois cartes " +
                                "retournées du sommet de la pioche lors de la traversée d'un Tunnel oblige le joueur à rajouter une carte wagon à sa série s’il souhaite s'emparer de " +
                                "cette route. " +
                                "Si un joueur décide de traverser un Tunnel en jouant uniquement des cartes Locomotive, il ne devra jouer des cartes supplémentaires que si des " +
                                "Locomotives figurent parmi les trois cartes tirées du sommet de la pioche de cartes Wagon. " +
                                "Dans le cas peu probable où la pioche et la défausse seraient toutes deux simultanément épuisées ou insuffisantes pour permettre de retourner trois " +
                                "cartes Wagon, ne sont alors retournées que les cartes disponibles. Si, en raison d'une accumulation des cartes Wagon par les joueurs, il ne reste aucune " +
                                "carte disponible, le Tunnel peut alors être emprunté sans courir le risque de devoir ajouter des cartes au coût initial de sa traversée.");
                    }
                    case "Prendre des cartes Destination" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Un joueur peut utiliser son tour de jeu pour piocher des cartes Destination supplémentaires. Pour cela, il doit prendre " +
                                "3 cartes sur le dessus de la pile de cartes Destination. S’il reste moins de 3 cartes Destination dans la pioche, le " +
                                "joueur ne tire que le nombre de cartes restantes. " +
                                "Il doit conserver au moins l’une des trois cartes piochées, mais peut bien sûr en garder 2 ou même 3. Chaque carte " +
                                "non conservée est remise face cachée sous la pioche de cartes Destination. Toute carte conservée doit l'être jusqu'à " +
                                "la fin du jeu, et ne peut être abandonnée ni défaussée à l'occasion d'une autre pioche. " +
                                "Les villes figurant sur une carte Destination sont le point de départ et d'arrivée d'un objectif secret que le joueur " +
                                "s'efforce d’atteindre en reliant ses villes avec des trains de sa couleur avant la fin de la partie. En cas de réussite, " +
                                "les points figurant sur la carte Destination sont gagnés par le joueur lorsque les objectifs sont révélés et " +
                                "comptabilisés en fin de partie. En cas d'échec, ces points sont déduits du score du joueur en fin de partie. " +
                                "Au cours d'une partie, un joueur peut piocher et conserver autant de cartes Destination qu’il le souhaite.");
                    }
                    case "Bâtir une gare" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Une Gare permet à son propriétaire d'emprunter une et une seule des routes " +
                                "appartenant à l'un de ses opposants pour relier des villes objectifs figurant sur ses " +
                                "propres cartes Destination. " +
                                "Les Gares peuvent être construites sur n'importe quelle ville vierge, même s’il n'y a " +
                                "alors aucune route reliant déjà cette ville à une autre. Deux joueurs ne peuvent " +
                                "jamais bâtir deux Gares sur une même ville. " +
                                "Chaque joueur ne peut construire qu'une seule Gare par tour, et trois tout au plus sur " +
                                "l'ensemble de sa partie. " +
                                "Pour bâtir sa première Gare, le joueur joue et se défausse d'une carte, et place une " +
                                "Gare sur la ville de son choix. Pour bâtir sa seconde Gare, le joueur doit présenter et " +
                                "défausser deux cartes d'une même couleur ; pour bâtir sa troisième Gare, trois cartes " +
                                "d'une même couleur devront être jouées et défaussées. Comme toujours, il est " +
                                "possible de remplacer certaines des cartes jouées par des Locomotives. " +
                                "Si un joueur se sert d'une même Gare pour relier plusieurs villes correspondant à des objectifs différents, il est néanmoins obligé d'emprunter la même " +
                                "route reliée à cette Gare à chaque fois. C'est seulement une fois la partie achevée que le joueur décide de la voie unique qu'il souhaite emprunter à " +
                                "partir de chacune de ses Gares. " +
                                "Un joueur n'est jamais obligé de bâtir une Gare. Chaque Gare non bâtie et gardée en réserve rapporte quatre points à son propriétaire en fin de partie.");
                    }
                    case "Décompte des points" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Les joueurs devraient déjà avoir calculé les points gagnés " +
                                "lors de leur prise de possession de routes sur le plateau de " +
                                "jeu. Afin de s’assurer qu’il n’y a pas d’erreur, il est conseillé " +
                                "de faire un décompte de vérification, en reprenant pour " +
                                "chaque joueur chacune de ses routes et en additionnant les " +
                                "points correspondants. " +
                                "Chaque joueur doit alors révéler ses cartes Destination et " +
                                "ajouter (ou soustraire) la valeur de chacune d'entre elles à " +
                                "son score. Si la connexion entre les deux villes est réussie, " +
                                "on ajoute, si elle a échoué, on soustrait. " +
                                "Chaque Gare construite permet à son propriétaire " +
                                "d'emprunter une et une seule route reliant la ville sur laquelle " +
                                "celle-ci est bâtie, afin de remplir un ou des objectifs. Si ceuxci sont multiples, la voie empruntée grâce à cette Gare doit " +
                                "néanmoins rester la même. " +
                                "N'oubliez pas non plus que chaque Gare non bâtie rapporte maintenant quatre points à son propriétaire. " +
                                "Enfin, le joueur qui a le chemin continu le plus long reçoit la carte bonus European Express du chemin le plus long et marque 10 points supplémentaires. " +
                                "Pour comparer les longueurs des chemins de chacun des joueurs, il convient de ne prendre en compte que les seuls trains en plastique de ces joueurs, " +
                                "et non les voies que les Gares leur permettent d'emprunter. Lors de ce calcul, les boucles sont autorisées (le chemin le plus long peut passer plusieurs " +
                                "fois par la même ville) mais en aucun cas un même train en plastique ne peut être comptabilisé ni utilisé deux fois. En cas d’égalité entre plusieurs " +
                                "joueurs, tous les joueurs concernés marquent 10 points. " +
                                "Le joueur qui a le plus grand total de points est alors déclaré vainqueur. En cas d’égalité entre plusieurs joueurs, le joueur qui a complété le plus de " +
                                "cartes Destination remporte la victoire. En cas d'égalité répétée, celui qui a construit le moins de Gares est déclaré vainqueur. Dans le cas peu " +
                                "probable où il y aurait encore égalité, le joueur qui a le chemin le plus long gagne.");
                    }
                }
            };

            EventHandler<ActionEvent> actionEventEventHandler = actionEvent12 -> {
                switch (((MenuItem)actionEvent12.getSource()).getText()){

                    case "Raccourci" -> {
                        page.setText("-Appuyer sur la touche ECHAP/ESC de votre clavier afin d'afficher le menu pause \n");
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                    }
                    case "Aides" -> {
                        if (!pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().add(pane);
                        if (pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().remove(page);
                    }
                }
            };


            raccourci.setOnAction(actionEventEventHandler);
            aides.setOnAction(actionEventEventHandler);
            depart.setOnAction(action);
            cartesWagons.setOnAction(action);
            routes.setOnAction(action);
            routesSpe.setOnAction(action);
            cartesDestinations.setOnAction(action);
            gares.setOnAction(action);
            points.setOnAction(action);

            Scene deuxiemeScene = new Scene(window,1280,720);


            reglesWindow.setResizable(false);
            reglesWindow.setTitle("Regles du jeu");
            reglesWindow.setScene(deuxiemeScene);

            buttonResume.setOnAction(actionEvent2 -> {
                pauseWindow.setDisable(false);
                reglesWindow.close();
            });

            ChangeListener<Boolean> fenetreMontre = (observableValue, aBoolean, t1) -> {
                if(!observableValue.getValue()){
                    pauseWindow.setDisable(false);
                    timer.play();
                }
            };

            reglesWindow.showingProperty().addListener(fenetreMontre);

            reglesWindow.show();
        });

        Platform.runLater(() -> {
            borderPlateau.setStroke(Color.valueOf(couleurWagonStringHashMap.get(jeu.joueurCourantProperty().getValue().getCouleur())));
        });

        time.textProperty().bind(Bindings.createStringBinding(()-> getTimeStringFromDuration(timeleft.getValue()),timeleft));

        piocheCarteW.setOnMouseClicked(actionEvent -> {
            jeu.uneCarteWagonAEtePiochee();
        });
        piocheDestination.setOnMouseClicked(actionEvent -> {
            jeu.uneDestinationAEtePiochee();
        });

        piocheCarteW.setOnMouseEntered(actionEvent -> {
            piocheCarteW.getChildren().add(iconeSurvol);
        });

        piocheDestination.setOnMouseEntered(actionEvent -> {
            piocheDestination.getChildren().add(iconeSurvol);
        });

        piocheCarteW.setOnMouseExited(actionEvent -> {
            piocheCarteW.getChildren().remove(iconeSurvol);
        });

        piocheDestination.setOnMouseExited(actionEvent -> {
            piocheDestination.getChildren().remove(iconeSurvol);
        });

        destinationsC.setOnMouseEntered(actionEvent -> {
            destinationsC.getChildren().add(iconeSurvol);
        });

        destinationsC.setOnMouseExited(actionEvent -> {
            destinationsC.getChildren().remove(iconeSurvol);
        });

        ChangeListener<String> changeInstruction = (observableValue, stringObjectProperty, t1) -> {
            if (console.getChildren().size() > 5){
                console.getChildren().clear();
            }
            Label tampon = new Label("-" + jeu.instructionProperty().getValue());
            tampon.setWrapText(true);
            tampon.setFont(new Font("Cambria", 12));
            if (console.getChildren().size() == 0){
                tampon.setPadding(new Insets(20,0,0,15));
            }else{
                tampon.setPadding(new Insets(0,0,0,15));
            }
            console.getChildren().add(tampon);
        };

        jeu.instructionProperty().addListener(changeInstruction);

        ListChangeListener<? super CouleurWagon> changeListListener = observable ->{
            Platform.runLater(() -> {
                cliquer.setValue(false);
                while (observable.next()){
                    if (observable.wasAdded()){
                        for (CouleurWagon cw : observable.getAddedSubList()){
                            VueCarteWagon carte = new VueCarteWagon(cw, null, true);
                            if (!this.cartesJoueurCourant.getChildren().contains(carte)){
                                    carte.incrementer(Collections.frequency(observable.getAddedSubList(), cw));
                                this.cartesJoueurCourant.getChildren().add(carte);
                            }else{
                                for (Node vcw : cartesJoueurCourant.getChildren()){
                                    if (carte.equals(vcw)){
                                        ((VueCarteWagon) vcw).incrementer(Collections.frequency(observable.getAddedSubList(), cw));
                                    }
                                }
                            }
                        }
                    }
                }
            });
        };


        for (IJoueur joueur : jeu.getJoueurs()){
            joueur.cartesWagonProperty().addListener(changeListListener);
        }

        ChangeListener<IJoueur> changeListener = (observableValue, old, nouveau) -> {
            Platform.runLater(() -> {
                cliquer.setValue(false);
                this.cartesJoueurCourant.getChildren().clear();
                for (CouleurWagon cw : observableValue.getValue().getCartesWagon()){
                    VueCarteWagon carte = new VueCarteWagon(cw, null, true);
                    if (!this.cartesJoueurCourant.getChildren().contains(carte)){
                        carte.incrementer(Collections.frequency(observableValue.getValue().getCartesWagon(), cw));
                        this.cartesJoueurCourant.getChildren().add(carte);
                    }
                }
                listeCartesWagonsVisible.setDisable(false);
                listeCartesWagonsVisible.setOpacity(1.0);
                pioches.setDisable(false);
                pioches.setOpacity(1.0);
                destination.setDisable(false);
                destination.setOpacity(1.0);
                borderPlateau.setStroke(Color.valueOf(couleurWagonStringHashMap.get(jeu.joueurCourantProperty().getValue().getCouleur())));
            });
        };

        this.jeu.joueurCourantProperty().addListener(changeListener);

        ListChangeListener<? super Destination> listenerDestination = observable -> {
            Platform.runLater(() ->{
                while (observable.next()) {
                    if (observable.wasAdded()) {
                        for (int i = 0; i < observable.getAddedSubList().size(); i++) {
                            System.out.println(observable.getAddedSubList().get(i).toString());
                            this.listeDestinationBox.getChildren().add(new VueDestination(observable.getAddedSubList().get(i), false));
                        }
                    }
                    if (observable.wasRemoved()) {
                        for (int i = 0; i < observable.getRemoved().size(); i++) {
                            System.out.println(observable.getRemoved().get(i).toString());
                            this.listeDestinationBox.getChildren().remove((trouveLabelDestination(observable.getRemoved().get(i))));
                        }
                    }
                    if (observable.wasUpdated()) {
                        for (int i = observable.getFrom(); i < observable.getTo(); i++) {
                            System.out.println(observable.toString());
                        }
                    }
                }
                for (Node dest : listeDestinationBox.getChildren()){
                    ((VueDestination) dest).creerBindings();
                }

            });
        };
        this.jeu.destinationsInitialesProperty().addListener(listenerDestination);


        this.passer.setOnAction(actionEvent -> {
            System.out.println("Le joueur a passé son tour");
            if (newWindow.isShowing()) newWindow.close();
            this.jeu.passerAEteChoisi();
        });

        this.boutonPause.setOnAction(actionEvent -> {
            affichePagePause();
        });


        ListChangeListener<? super CouleurWagon> listenerCartesVisibles = observable -> {
            Platform.runLater(() -> {
                while (observable.next()){
                    if(observable.wasAdded()){
                        for(int i = 0; i < observable.getAddedSubList().size(); i++){
                            this.listeCartesWagonsVisible.getChildren().add(new VueCarteWagon(observable.getAddedSubList().get(i),270.0, false));
                        }
                    }
                    if(observable.wasRemoved()){
                        for (int i = 0; i < observable.getRemoved().size(); i++) {
                            System.out.println(observable.getRemoved().get(i));
                            this.listeCartesWagonsVisible.getChildren().remove(trouveAfficheCouleurWagon(observable.getRemoved().get(i)));
                        }
                    }
                    if(observable.wasUpdated()){

                    }
                }
                for(int i = 0; i < listeCartesWagonsVisible.getChildren().size(); i++){
                    VueCarteWagon courant = (VueCarteWagon) this.listeCartesWagonsVisible.getChildren().get(i);
                    courant.creerBindings();
                }
            });
        };
        this.jeu.cartesWagonVisiblesProperty().addListener(listenerCartesVisibles);

        afficherNouvellePageDestination();

        getScene().setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ESCAPE){
                affichePagePause();
            }
        });


        ChangeListener<? super Number> changeX = (observable, s, t1) -> {
            Platform.runLater(() -> {
                resizeFenetre(this);
            });
        };




        ChangeListener<? super Number> changeY = (observable, s, t1) -> {
            Platform.runLater(() -> {
                resizeFenetre(this);
            });
        };




        Platform.runLater(() -> {
            getScene().getWindow().widthProperty().addListener(changeX);
        });

        Platform.runLater(() -> {
            getScene().getWindow().heightProperty().addListener(changeY);
        });



    }

    /*
    private void resize(Pane pane, double ratio){
        for (Node n : pane.getChildren()){
            n.setScaleX(ratio);
            n.setScaleY(ratio);
            if (existeEnfants(n) != null) resize(existeEnfants(n), ratio);
        }
    }

     */


    public void affichePagePause(){
        timer.stop();
        this.setOpacity(0.5);
        this.setDisable(true);

        pauseWindow = new StackPane();

        CustomButton button = new CustomButton("Resume",200);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(button,button1);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(30);

        pauseWindow.getChildren().addAll(menuPause, vBox);
        pauseWindow.setAlignment(Pos.CENTER);
        pauseWindow.setBackground(new Background(new BackgroundImage(
                new Image("file:src/main/resources/images/a74329729a28555809ef11b2498a9b2c.jpg"),
                BackgroundRepeat.REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT)
        ));

        Scene deuxiemeScene = new Scene(pauseWindow, 423, 640);

        pause.setResizable(false);
        pause.setTitle("Menu pause");
        pause.setScene(deuxiemeScene);

        button.setOnAction(actionEvent -> {
            this.setOpacity(1);
            this.setDisable(false);
            timer.play();
            reglesWindow.close();
            pause.close();
        });

        ChangeListener<Boolean> fenetreMontre = (observableValue, aBoolean, t1) -> {
            if(!observableValue.getValue()){
                this.setOpacity(1);
                this.setDisable(false);
                reglesWindow.close();
                timer.play();
            }
        };
        pause.showingProperty().addListener(fenetreMontre);

        pause.show();
    }

    private void resizeFenetre(Pane pane){ //wORh = 0 pour Width et 1 pour Height
        for (Node n : pane.getChildren()){
            double ratioX = getScene().getWindow().getWidth() / 1920;
            double ratioY = getScene().getWindow().getHeight() / 1080;


            if (ratioX <= 0.8 && ratioX > 0.4 || ratioY <= 0.8 && ratioY > 0.4){
                n.setScaleX(ratioX+0.2);
                n.setScaleY(ratioY+0.2);
            }else if (ratioX <= 0.4 && ratioX > 0.2 || ratioY <= 0.4 && ratioY > 0.2){
                n.setLayoutX(ratioX+0.4);
                n.setScaleY(ratioY+0.4);
            }else if (ratioX <= 0.2 || ratioY <= 0.2){
                n.setScaleX(ratioX+0.6);
                n.setScaleY(ratioY+0.6);
            }else if (ratioX >= 1.0 || ratioY >= 1.0){
                n.setScaleX(1.0);
                n.setScaleY(1.0);
            }else{
                n.setScaleX(ratioX);
                n.setScaleY(ratioY);
            }
            if (existeEnfants(n) != null) resizeFenetre(existeEnfants(n));
        }
    }




    private Pane existeEnfants(Node n){
        if (n instanceof Pane) {
            return (Pane) n;
        }
        return null;
    }





    private VueDestination trouveLabelDestination(IDestination destination){
        for(int i = 0; i < this.listeDestinationBox.getChildren().size(); i++){
            VueDestination courant = (VueDestination) this.listeDestinationBox.getChildren().get(i);
            if(courant.getDestination().equals(destination)){
                return courant;
            }
        }
        return null;
    }

    private VueCarteWagon trouveAfficheCouleurWagon(ICouleurWagon couleurWagon){
        for(int i = 0; i < this.listeCartesWagonsVisible.getChildren().size(); i++){
            VueCarteWagon courant = (VueCarteWagon) this.listeCartesWagonsVisible.getChildren().get(i);
            if(courant.getCouleurWagon().equals(couleurWagon)){
                return courant;
            }
        }
        return null;
    }

    private void afficherNouvellePageDestination(){
        this.destination.setOnMouseClicked(actionEvent ->{
            destinationsCourante.getChildren().clear();
            ArrayList<Destination> destinationsCourantes = new ArrayList<>(((VueDuJeu) getScene().getRoot()).getJeu().joueurCourantProperty().getValue().getDestinations());
            for(Destination d : destinationsCourantes){
                VueDestination destination = new VueDestination(d, true);
                destinationsCourante.getChildren().add(destination);
            }
            Pane pane = new Pane();
            pane.getChildren().add(destinationsCourante);
            BackgroundImage backgroundImage = new BackgroundImage(
                    new Image("file:src/main/resources/images/destinations.png"),
                    BackgroundRepeat.NO_REPEAT,
                    BackgroundRepeat.NO_REPEAT,
                    BackgroundPosition.DEFAULT,
                    BackgroundSize.DEFAULT);
            Background background = new Background(backgroundImage);
            pane.setBackground(new Background(new BackgroundFill(Color.GRAY,null,null)));

            Scene deuxiemeScene = new Scene(pane, destinationsCourante.getPrefWidth(), destinationsCourante.getPrefHeight());

            newWindow = new Stage();
            newWindow.setResizable(false);
            newWindow.setTitle("Informations sur les destinations de " + ((VueDuJeu) getScene().getRoot()).getJeu().joueurCourantProperty().getValue().getNom());
            newWindow.setScene(deuxiemeScene);

            newWindow.show();
        });


    }



}