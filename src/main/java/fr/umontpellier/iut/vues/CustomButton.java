package fr.umontpellier.iut.vues;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;

public class CustomButton extends Button {

    public CustomButton(String nom, double width){
        super(nom);
        this.setTextFill(Color.WHITE);
        this.setPadding(new Insets(8,50,15,50));
        this.setMaxWidth(width);
        this.setStyle("-fx-background-color:" +
                "linear-gradient(from 0% 93% to 0% 100%, #a34313 0%, #903b12 100%)," +
                "#9d4024," +
                "#d86e3a," +
                "radial-gradient(center 50% 50%, radius 100%, #d86e3a, #c54e2c);" +
                "-fx-effect:" + "dropshadow( gaussian , rgba(0,0,0,0.75) , 4,0,0,1 );" +
                "-fx-font-weight: bold;" +
                "-fx-font-size: 1.1em;" +
                "-fx-background-radius: 8;" +
                "-fx-background-insets: 0, 0 0 5 0, 0 0 6 0, 0 0 7 0;");

        this.setOnMouseEntered(actionEvent -> {
            this.setMaxWidth(this.getWidth()-5);
        });

        this.setOnMouseExited(actionEvent -> {
            this.setMaxWidth(this.getWidth()+5);
        });
    }
}