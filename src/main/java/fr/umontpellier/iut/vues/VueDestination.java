package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.IDestination;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 * Cette classe représente la vue d'une carte Destination.
 *
 * On y définit le listener à exécuter lorsque cette carte a été choisie par l'utilisateur
 */
public class VueDestination extends HBox {

    private IDestination destination;
    private StackPane carte;
    private Rectangle iconeSurvol;
    private Rectangle imgCarte;

    public VueDestination(IDestination destination, boolean vueDestinationsJoueur) {
        this.destination = destination;
        if (vueDestinationsJoueur) imgCarte = new Rectangle(240,140);
        else imgCarte = new Rectangle(120,70);
        carte = new StackPane();
        getChildren().add(carte);
        imgCarte.setFill(new ImagePattern(new Image("file:src/main/resources/images/missions/eu-" + parseVille1(destination.getNom()).toLowerCase() + "-" + parseVille2(tabParseVille1(destination.getNom())).toLowerCase() + ".png")));
        iconeSurvol = new Rectangle(50,50);
        iconeSurvol.setFill(new ImagePattern(new Image("file:src/main/resources/images/main.png")));
        carte.getChildren().add(imgCarte);
    }

    public IDestination getDestination() {
        return destination;
    }

    public String parseVille1(String nomDestination){
        String[] parse = destination.getNom().split("\\s+");
        return parse[0];
    }

    public String[] tabParseVille1(String nomDestination){
        return destination.getNom().split("-");
    }

    public String parseVille2(String[] parser){
        String[] parse = parser[1].split("\\s+");
        return parse[1];
    }

    public void creerBindings(){
        this.setOnMouseClicked(actionEvent -> {
            ((VueDuJeu) getScene().getRoot()).getJeu().uneDestinationAEteChoisie(destination.getNom());
        });
        this.setOnMouseEntered(actionEvent -> {
            carte.getChildren().add(iconeSurvol);
        });

        this.setOnMouseExited(actionEvent -> {
            carte.getChildren().remove(iconeSurvol);
        });

    }


}