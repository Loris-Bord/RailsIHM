package fr.umontpellier.iut.vues;

import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.w3c.dom.Text;

import javax.swing.*;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class SaisieNomJoueur extends Pane {

    @FXML
    private ComboBox<Integer> listeDeNombreDeJoueurs;
    @FXML
    private TextField joueur1;
    @FXML
    private TextField joueur2;
    @FXML
    private TextField joueur3;
    @FXML
    private TextField joueur4;
    @FXML
    private TextField joueur5;

    @FXML
    private Button startGame;

    @FXML
    private VBox box;

    private Rectangle iconeRegles;

    private HBox regles;

    private Label textRegles;

    private ArrayList<TextField> nomsJoueurs;

    private Rectangle iconeSurvol;

    private StackPane imgRegles;

    public SaisieNomJoueur(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/initJoueurs.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
            listeDeNombreDeJoueurs.getItems().setAll(1,2,3,4,5);
            initHasMap();
        } catch (IOException e) {
            e.printStackTrace();
        }
        regles = new HBox();
        regles.setPadding(new Insets(0,0,0,80));
        textRegles = new Label("Voir les règles : ");
        iconeSurvol = new Rectangle(35,35);
        imgRegles = new StackPane();
        iconeRegles = new Rectangle(80,80);
        iconeRegles.setFill(new ImagePattern(new Image("file:src/main/resources/images/regles.png")));
        imgRegles.getChildren().add(iconeRegles);
        iconeSurvol.setFill(new ImagePattern(new Image("file:src/main/resources/images/main.png")));
        regles.getChildren().addAll(textRegles,imgRegles);
        regles.setAlignment(Pos.CENTER);
        textRegles.setFont(new Font("Cambria", 20));
        textRegles.setTextFill(Color.BLACK);
        textRegles.setStyle("-fx-font-weight: bold");
        box.getChildren().add(regles);
    }

    private void initHasMap(){
        nomsJoueurs = new ArrayList<>();
        nomsJoueurs.add(joueur1);
        nomsJoueurs.add(joueur2);
        nomsJoueurs.add(joueur3);
        nomsJoueurs.add(joueur4);
        nomsJoueurs.add(joueur5);
    }

    public ComboBox<Integer> getListeDeNombreDeJoueurs(){
        return listeDeNombreDeJoueurs;
    }

    public ArrayList<TextField> getNomsJoueurs() {
        return nomsJoueurs;
    }

    public Button getStartGame(){
        return startGame;
    }

    public void creerBindings(){
        imgRegles.setOnMouseEntered(actionEvent -> {
            imgRegles.getChildren().add(iconeSurvol);
        });

        imgRegles.setOnMouseExited(actionEvent -> {
            imgRegles.getChildren().remove(iconeSurvol);
        });



        imgRegles.setOnMouseClicked(actionEvent -> {
            this.setDisable(true);
            CustomButton buttonResume = new CustomButton("Revenir",200);
            VBox window = new VBox();
            Label page = new Label("Choissisez une page");
            page.setFont(new Font("Cambria", 20));
            page.setStyle("-fx-font-weight: bold");
            page.setTextFill(Color.BLACK);
            page.setWrapText(true);
            page.setMaxWidth(1000);
            page.setTranslateX(100);
            VBox buttons = new VBox();
            HBox pageConteneur = new HBox();
            HBox.setHgrow(pageConteneur, Priority.ALWAYS);
            buttons.setAlignment(Pos.CENTER);
            buttons.getChildren().add(buttonResume);
            MenuBar menuBar = new MenuBar();
            Menu choix = new Menu("Règles du jeu");
            Menu raccourciAides = new Menu("Raccourci et aides");
            MenuItem raccourci = new MenuItem("Raccourci");
            MenuItem aides = new MenuItem("Aides");
            MenuItem depart = new MenuItem("Tour de jeu");
            MenuItem cartesWagons = new MenuItem("Prendre des cartes wagons");
            MenuItem routes = new MenuItem("Prendre possession des routes");
            MenuItem routesSpe = new MenuItem("Ferries/Tunnels");
            MenuItem cartesDestinations = new MenuItem("Prendre des cartes Destination");
            MenuItem gares = new MenuItem("Bâtir une gare");
            MenuItem points = new MenuItem("Décompte des points");
            choix.getItems().addAll(depart,cartesWagons,routes,routesSpe,cartesDestinations,gares,points);
            raccourciAides.getItems().addAll(raccourci,aides);
            pageConteneur.getChildren().add(page);
            window.getChildren().addAll(menuBar, buttons, pageConteneur);
            menuBar.getMenus().addAll(choix,raccourciAides);
            window.setBackground(new Background(
                    new BackgroundImage(new Image("file:src/main/resources/images/console.png"),
                            BackgroundRepeat.NO_REPEAT,
                            BackgroundRepeat.NO_REPEAT,
                            BackgroundPosition.CENTER,
                            new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, true, true, true,true))));

            Rectangle pane = new Rectangle(1050,600);
            pane.setTranslateX(100);
            pane.setFill(new ImagePattern(new Image("file:src/main/resources/images/aides-regles.png")));

            EventHandler<ActionEvent> action = actionEvent1 -> {
                switch (((MenuItem) actionEvent1.getSource()).getText()){
                    case "Tour de jeu" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Le joueur ayant visité le plus grand nombre de pays européens commence. La partie " +
                                "continue ensuite dans le sens des aiguilles d'une montre. À son tour, le joueur doit " +
                                "faire une, et une seule, des quatre actions suivantes : " +
                                "Prendre des cartes Wagon – Le joueur peut prendre deux cartes Wagon (ou une seule, " +
                                "s’il choisit de prendre une Locomotive, face visible. Se référer à la section Locomotives " +
                                "pour plus de détails) ; " +
                                "Prendre possession d'une Route – Le joueur peut s'emparer d'une route sur le " +
                                "plateau en posant autant de cartes Wagon de la couleur de la route que d'espaces " +
                                "composant cette route. Après avoir défaussé ses cartes, le joueur pose ses wagons " +
                                "sur chaque espace constituant la route, et déplace son marqueur de score d'un " +
                                "nombre de cases correspondant au barème de décompte des points ; " +
                                "Prendre des cartes Destination supplémentaires – Le joueur tire trois cartes " +
                                "Destination du dessus de la pioche. Il doit en conserver au moins une, et remettre " +
                                "celles qu'il ne souhaite pas garder en dessous de la pioche ; " +
                                "Construire une Gare – Le joueur peut bâtir une Gare sur toute ville qui en est jusque " +
                                "là dépourvue. S'il s'agit de sa première Gare, le joueur se défausse d'une carte Wagon, " +
                                "et place sa Gare. S’il s'agit de sa seconde Gare, le joueur doit se défausser de deux " +
                                "cartes de même couleur pour la bâtir. Et s’il s'agit de sa troisième Gare, il doit se " +
                                "défausser de trois cartes de couleur identique. ");
                    }
                    case "Prendre des cartes wagons" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Il existe 8 types de wagons différents en plus des Locomotives. Les couleurs de " +
                                "chaque carte Wagon correspondent aux couleurs des routes présentes sur le plateau " +
                                "connectant des villes – bleu, violet, orange, blanc, vert, jaune, noir et rouge. " +
                                "Lorsqu'un joueur choisit de prendre des cartes Wagon, il peut en piocher jusqu'à deux " +
                                "par tour. Chacune de ces cartes doit être soit prise parmi les cinq faces visibles, soit tirée du dessus de la pioche (tirage à l'aveugle). Lorsqu'une carte " +
                                "face visible est choisie, elle doit être immédiatement remplacée par une nouvelle carte tirée du sommet de la pioche. " +
                                "Si le joueur choisit de prendre une Locomotive face visible parmi les cinq cartes exposées, il ne peut piocher d'autre carte lors de ce tour (pour plus " +
                                "de détails, se référer à la section sur les Locomotives). Si, à un moment quelconque, trois des cinq cartes face visible sont des Locomotives, les cinq " +
                                "cartes sont immédiatement mises de côté et remplacées par cinq nouvelles cartes face visible. " +
                                "Un joueur peut conserver en main autant de cartes qu'il le souhaite durant la partie. Si la pioche est épuisée, la défausse est mélangée pour constituer " +
                                "une nouvelle pioche. Veillez à bien mélanger celle-ci, les cartes étant généralement défaussées par séries de couleur. " +
                                "Dans le cas peu probable où la pioche et la défausse seraient simultanément épuisées car les joueurs ont gardé toutes les cartes en main, le joueur " +
                                "dont c’est le tour ne pourrait choisir de piocher des cartes Wagon ; il se retrouvera alors contraint de choisir une des trois autres options : prendre " +
                                "possession d'une route, piocher de nouvelles cartes Destination, ou bâtir une nouvelle Gare. " +
                                "Locomotives " +
                                "Les Locomotives sont multicolores et, comme des cartes Joker, peuvent remplacer n'importe quelle couleur lors de " +
                                "la prise de possession d'une route. Les Locomotives sont également nécessaires pour emprunter des Ferries " +
                                "(cf. section sur les Ferries). " +
                                "Si une carte Locomotive figure parmi les cinq cartes visibles lors d'une phase de pioche de cartes Wagon, le joueur " +
                                "peut la prendre, mais son tour s’arrête alors immédiatement. La Locomotive compte comme s’il avait pris 2 cartes. Si après avoir pris une carte visible " +
                                "(qui n’est pas une Locomotive), la carte de remplacement tirée est une Locomotive, elle ne peut donc être prise par ce joueur ce tour-ci. " +
                                "Si par contre un joueur a la chance de tirer une Locomotive en aveugle, au sommet de la pioche, les autres joueurs n'en savent rien, et cette carte ne compte " +
                                "que pour une carte piochée. Il peut alors tirer une deuxième carte (en aveugle, ou une carte non Locomotive, parmi les cinq cartes face visible). ");
                    }
                    case "Prendre possession des routes" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Une route est une ligne continue de rectangles colorés ou gris reliant " +
                                "deux villes adjacentes sur le plateau de jeu. Pour prendre possession " +
                                "d'une route, le joueur doit jouer des cartes Wagon de la couleur " +
                                "correspondante, en quantité suffisante pour égaler le nombre d'espaces " +
                                "figurant sur la route convoitée. " +
                                "La plupart des routes nécessitent une série de cartes de couleur " +
                                "spécifique. Des cartes Locomotive peuvent être utilisées pour remplacer " +
                                "des cartes manquantes dans la couleur désirée (exemple 1). " +
                                "Certaines routes – en gris sur le plateau - peuvent être capturées en " +
                                "utilisant n’importe quelle série d’une même couleur (exemple 2). " +
                                "Lorsqu’une route a été capturée, le joueur pose ses wagons en plastique " +
                                "sur chacun des espaces qui constituent la route. Toutes les cartes " +
                                "utilisées pour s’approprier cette route sont défaussées. Le joueur avance " +
                                "alors immédiatement son marqueur de score d'un nombre de cases " +
                                "correspondant au barème indiqué sur le tableau de décompte des points " +
                                "en page 7 de ce livret. " +
                                "Un joueur peut prendre possession de n'importe quelle route sur le " +
                                "plateau de jeu. Il n'est pas obligé de se connecter avec des routes déjà à " +
                                "son actif. " +
                                "Une route doit être capturée dans son intégralité en un tour de jeu. Il est " +
                                "impossible, par exemple, de poser deux trains sur une route longue de " +
                                "trois espaces, puis d'attendre le tour suivant pour jouer une troisième " +
                                "carte et poser son dernier train. " +
                                "Un joueur ne peut jamais capturer plus d'une route par tour de jeu. " +
                                "Routes Doubles " +
                                "Certaines villes sont connectées par des routes doubles. Ces routes sont parallèles de bout en bout, et " +
                                "de longueur identique. Elles connectent obligatoirement les mêmes villes pour les deux routes. " +
                                "Un même joueur ne peut jamais capturer les deux routes d'une même route double durant une partie. " +
                                "Attention : Certaines routes sont en partie parallèles, mais relient différentes villes. Il ne s'agit alors pas " +
                                "de routes doubles, mais simplement de routes (en partie) parallèles. " +
                                "Note importante : dans une configuration à 2 ou 3 joueurs, seule l’une des routes constituant la double connexion peut " +
                                "être utilisée. Un joueur peut prendre possession de l’une des deux routes disponibles, mais dès lors, la route restante " +
                                "demeurera fermée et inaccessible à tous jusqu’en fin de partie. ");
                    }
                    case "Ferries/Tunnels" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Ferries " +
                                "Les Ferries sont des routes spéciales, de couleur grise, reliant " +
                                "deux villes séparées par des eaux. Les lignes de Ferries sont " +
                                "facilement reconnaissables grâce au symbole de Locomotive " +
                                "figurant sur l'un au moins des espaces constituant cette route. " +
                                "Pour prendre possession d'une ligne de Ferries, un joueur doit jouer une " +
                                "carte Locomotive pour chaque symbole de Locomotive figurant sur la " +
                                "route. " +
                                "Tunnels " +
                                "Les Tunnels sont des routes spéciales, reconnaissables à leurs marques et contours " +
                                "spécifiques. " +
                                "Ce qui distingue le tunnel d'une route normale, c'est que le joueur qui l'emprunte n'est jamais " +
                                "sûr de la distance qu'il devra parcourir avant de rejoindre l'autre bout ! " +
                                "Quand il tente de traverser un Tunnel, le joueur joue d'abord le nombre de cartes de la couleur requise, " +
                                "comme à l’accoutumée. Puis il retourne les trois premières cartes du sommet de la pioche de cartes Wagon. " +
                                "Pour chaque carte ainsi révélée se trouvant être de la couleur des cartes utilisées pour traverser le Tunnel, " +
                                "le joueur doit maintenant rajouter une nouvelle carte Wagon de même couleur (ou une Locomotive). C'est " +
                                "uniquement une fois celles-ci rajoutées que le joueur peut prendre possession du Tunnel. " +
                                "Si le joueur ne possède plus assez de cartes de la couleur correspondante (ou ne souhaite pas les jouer " +
                                "maintenant), il reprend toutes ses cartes en main et passe son tour. " +
                                "En fin de tour, les cartes tirées du sommet de la pioche sont défaussées. " +
                                "Attention : N'oubliez pas que les Locomotives sont des cartes multicolores. En conséquence, toute Locomotive figurant parmi les trois cartes " +
                                "retournées du sommet de la pioche lors de la traversée d'un Tunnel oblige le joueur à rajouter une carte wagon à sa série s’il souhaite s'emparer de " +
                                "cette route. " +
                                "Si un joueur décide de traverser un Tunnel en jouant uniquement des cartes Locomotive, il ne devra jouer des cartes supplémentaires que si des " +
                                "Locomotives figurent parmi les trois cartes tirées du sommet de la pioche de cartes Wagon. " +
                                "Dans le cas peu probable où la pioche et la défausse seraient toutes deux simultanément épuisées ou insuffisantes pour permettre de retourner trois " +
                                "cartes Wagon, ne sont alors retournées que les cartes disponibles. Si, en raison d'une accumulation des cartes Wagon par les joueurs, il ne reste aucune " +
                                "carte disponible, le Tunnel peut alors être emprunté sans courir le risque de devoir ajouter des cartes au coût initial de sa traversée.");
                    }
                    case "Prendre des cartes Destination" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Un joueur peut utiliser son tour de jeu pour piocher des cartes Destination supplémentaires. Pour cela, il doit prendre " +
                                "3 cartes sur le dessus de la pile de cartes Destination. S’il reste moins de 3 cartes Destination dans la pioche, le " +
                                "joueur ne tire que le nombre de cartes restantes. " +
                                "Il doit conserver au moins l’une des trois cartes piochées, mais peut bien sûr en garder 2 ou même 3. Chaque carte " +
                                "non conservée est remise face cachée sous la pioche de cartes Destination. Toute carte conservée doit l'être jusqu'à " +
                                "la fin du jeu, et ne peut être abandonnée ni défaussée à l'occasion d'une autre pioche. " +
                                "Les villes figurant sur une carte Destination sont le point de départ et d'arrivée d'un objectif secret que le joueur " +
                                "s'efforce d’atteindre en reliant ses villes avec des trains de sa couleur avant la fin de la partie. En cas de réussite, " +
                                "les points figurant sur la carte Destination sont gagnés par le joueur lorsque les objectifs sont révélés et " +
                                "comptabilisés en fin de partie. En cas d'échec, ces points sont déduits du score du joueur en fin de partie. " +
                                "Au cours d'une partie, un joueur peut piocher et conserver autant de cartes Destination qu’il le souhaite.");
                    }
                    case "Bâtir une gare" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Une Gare permet à son propriétaire d'emprunter une et une seule des routes " +
                                "appartenant à l'un de ses opposants pour relier des villes objectifs figurant sur ses " +
                                "propres cartes Destination. " +
                                "Les Gares peuvent être construites sur n'importe quelle ville vierge, même s’il n'y a " +
                                "alors aucune route reliant déjà cette ville à une autre. Deux joueurs ne peuvent " +
                                "jamais bâtir deux Gares sur une même ville. " +
                                "Chaque joueur ne peut construire qu'une seule Gare par tour, et trois tout au plus sur " +
                                "l'ensemble de sa partie. " +
                                "Pour bâtir sa première Gare, le joueur joue et se défausse d'une carte, et place une " +
                                "Gare sur la ville de son choix. Pour bâtir sa seconde Gare, le joueur doit présenter et " +
                                "défausser deux cartes d'une même couleur ; pour bâtir sa troisième Gare, trois cartes " +
                                "d'une même couleur devront être jouées et défaussées. Comme toujours, il est " +
                                "possible de remplacer certaines des cartes jouées par des Locomotives. " +
                                "Si un joueur se sert d'une même Gare pour relier plusieurs villes correspondant à des objectifs différents, il est néanmoins obligé d'emprunter la même " +
                                "route reliée à cette Gare à chaque fois. C'est seulement une fois la partie achevée que le joueur décide de la voie unique qu'il souhaite emprunter à " +
                                "partir de chacune de ses Gares. " +
                                "Un joueur n'est jamais obligé de bâtir une Gare. Chaque Gare non bâtie et gardée en réserve rapporte quatre points à son propriétaire en fin de partie.");
                    }
                    case "Décompte des points" -> {
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                        page.setText("Les joueurs devraient déjà avoir calculé les points gagnés " +
                                "lors de leur prise de possession de routes sur le plateau de " +
                                "jeu. Afin de s’assurer qu’il n’y a pas d’erreur, il est conseillé " +
                                "de faire un décompte de vérification, en reprenant pour " +
                                "chaque joueur chacune de ses routes et en additionnant les " +
                                "points correspondants. " +
                                "Chaque joueur doit alors révéler ses cartes Destination et " +
                                "ajouter (ou soustraire) la valeur de chacune d'entre elles à " +
                                "son score. Si la connexion entre les deux villes est réussie, " +
                                "on ajoute, si elle a échoué, on soustrait. " +
                                "Chaque Gare construite permet à son propriétaire " +
                                "d'emprunter une et une seule route reliant la ville sur laquelle " +
                                "celle-ci est bâtie, afin de remplir un ou des objectifs. Si ceuxci sont multiples, la voie empruntée grâce à cette Gare doit " +
                                "néanmoins rester la même. " +
                                "N'oubliez pas non plus que chaque Gare non bâtie rapporte maintenant quatre points à son propriétaire. " +
                                "Enfin, le joueur qui a le chemin continu le plus long reçoit la carte bonus European Express du chemin le plus long et marque 10 points supplémentaires. " +
                                "Pour comparer les longueurs des chemins de chacun des joueurs, il convient de ne prendre en compte que les seuls trains en plastique de ces joueurs, " +
                                "et non les voies que les Gares leur permettent d'emprunter. Lors de ce calcul, les boucles sont autorisées (le chemin le plus long peut passer plusieurs " +
                                "fois par la même ville) mais en aucun cas un même train en plastique ne peut être comptabilisé ni utilisé deux fois. En cas d’égalité entre plusieurs " +
                                "joueurs, tous les joueurs concernés marquent 10 points. " +
                                "Le joueur qui a le plus grand total de points est alors déclaré vainqueur. En cas d’égalité entre plusieurs joueurs, le joueur qui a complété le plus de " +
                                "cartes Destination remporte la victoire. En cas d'égalité répétée, celui qui a construit le moins de Gares est déclaré vainqueur. Dans le cas peu " +
                                "probable où il y aurait encore égalité, le joueur qui a le chemin le plus long gagne.");
                    }
                }
            };

            EventHandler<ActionEvent> actionEventEventHandler = actionEvent12 -> {
                switch (((MenuItem)actionEvent12.getSource()).getText()){

                    case "Raccourci" -> {
                        page.setText("-Appuyer sur la touche ECHAP/ESC de votre clavier afin d'afficher le menu pause \n");
                        if (!pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().add(page);
                        if (pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().remove(pane);
                    }
                    case "Aides" -> {
                        if (!pageConteneur.getChildren().contains(pane)) pageConteneur.getChildren().add(pane);
                        if (pageConteneur.getChildren().contains(page)) pageConteneur.getChildren().remove(page);
                    }
                }
            };


            raccourci.setOnAction(actionEventEventHandler);
            aides.setOnAction(actionEventEventHandler);
            depart.setOnAction(action);
            cartesWagons.setOnAction(action);
            routes.setOnAction(action);
            routesSpe.setOnAction(action);
            cartesDestinations.setOnAction(action);
            gares.setOnAction(action);
            points.setOnAction(action);

            Scene deuxiemeScene = new Scene(window,1280,720);

            Stage reglesWindow = new Stage();


            reglesWindow.setResizable(false);
            reglesWindow.setTitle("Regles du jeu");
            reglesWindow.setScene(deuxiemeScene);

            buttonResume.setOnAction(actionEvent2 -> {
                this.setDisable(false);
                reglesWindow.close();
            });

            ChangeListener<Boolean> fenetreMontre = (observableValue, aBoolean, t1) -> {
                if(!observableValue.getValue()){
                    this.setDisable(false);
                }
            };

            reglesWindow.showingProperty().addListener(fenetreMontre);

            reglesWindow.show();
        });

    }


}
