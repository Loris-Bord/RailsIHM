package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.rails.Plateau;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Cette classe correspond à une nouvelle fenêtre permettant de choisir le nombre et les noms des joueurs de la partie.
 *
 * Sa présentation graphique peut automatiquement être actualisée chaque fois que le nombre de joueurs change.
 * Lorsque l'utilisateur a fini de saisir les noms de joueurs, il demandera à démarrer la partie.
 */
public class VueChoixJoueurs extends Stage {

    private SaisieNomJoueur saisieNomJoueur = new SaisieNomJoueur();
    private Pane pane = new Pane();
    private Scene scene;
    private ObservableList<String> nomsJoueurs;


    private final Image img = new Image("file:src/main/resources/images/interfaceChoixJoueurs.png");
    private final BackgroundImage bgI = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.CENTER,
            new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, true, true, true, true));
    private final Background bg = new Background(bgI);

    public ObservableList<String> nomsJoueursProperty() {
        return nomsJoueurs;
    }

    public List<String> getNomsJoueurs() {
        return nomsJoueurs;
    }



    public VueChoixJoueurs() {
        saisieNomJoueur.creerBindings();
        nomsJoueurs = FXCollections.observableArrayList();
        pane.getChildren().add(saisieNomJoueur);
        pane.setBackground(bg);
        scene = new Scene(pane);
        this.setTitle("Choix des joueurs");
        this.centerOnScreen();
        this.setScene(scene);
        this.setResizable(false);
    }

    /**
     * Définit l'action à exécuter lorsque la liste des participants est correctement initialisée
     */
    public void setNomsDesJoueursDefinisListener(ListChangeListener<String> quandLesNomsDesJoueursSontDefinis) {
        ChangeListener<Integer> quandLeNombreDeJoueursChange = null;
        setChangementDuNombreDeJoueursListener(quandLeNombreDeJoueursChange);
        saisieNomJoueur.getStartGame().setDisable(true);
        creerBindings();
        this.nomsJoueurs.addListener(quandLesNomsDesJoueursSontDefinis);
    }


    private void setInvisibleForAll(){
        for (TextField entry : saisieNomJoueur.getNomsJoueurs()) {
            entry.visibleProperty().setValue(false);
            entry.clear();
        }
    }

    /**
     * Définit l'action à exécuter lorsque le nombre de participants change
     */
    protected void setChangementDuNombreDeJoueursListener(ChangeListener<Integer> quandLeNombreDeJoueursChange) {
        quandLeNombreDeJoueursChange = (observableValue, integer, t1) -> Platform.runLater(() -> {
            int i = t1;
            setInvisibleForAll();
            for (TextField text : saisieNomJoueur.getNomsJoueurs()){
                if (i <= 0) break;
                i--;
                text.setVisible(true);
            }
        });
        saisieNomJoueur.getListeDeNombreDeJoueurs().valueProperty().addListener(quandLeNombreDeJoueursChange);
    }

    /**
     * Vérifie que tous les noms des participants sont renseignés
     * et affecte la liste définitive des participants
     */
    protected void setListeDesNomsDeJoueurs() {
        ArrayList<String> tempNamesList = new ArrayList<>();
        for (int i = 1; i <= getNombreDeJoueurs() ; i++) {
            String name = getJoueurParNumero(i);
            if (name == null || name.equals("")) {
                tempNamesList.clear();
                break;
            }
            else
                tempNamesList.add(name);
        }
        if (!tempNamesList.isEmpty()) {
            hide();
            nomsJoueurs.clear();
            nomsJoueurs.addAll(tempNamesList);
        }
    }

    /**
     * Retourne le nombre de participants à la partie que l'utilisateur a renseigné
     */
    protected int getNombreDeJoueurs() {
        if (saisieNomJoueur.getListeDeNombreDeJoueurs().getValue() == null) return 0;
        else {
            return saisieNomJoueur.getListeDeNombreDeJoueurs().getValue();
        }
    }

    /**
     * Retourne le nom que l'utilisateur a renseigné pour le ième participant à la partie
     * @param playerNumber : le numéro du participant
     */
    protected String getJoueurParNumero(int playerNumber) {
        return saisieNomJoueur.getNomsJoueurs().get(playerNumber-1).getText();
    }


    private void creerBindings() {
        ChangeListener<Number> tousLesChampsSontRemplis = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                boolean cliquable = false;
                for(int i = 0; i < getNombreDeJoueurs(); i++){
                    if(saisieNomJoueur.getNomsJoueurs().get(i).getText().equals("")) cliquable = true;
                }
                saisieNomJoueur.getStartGame().setDisable(cliquable);
            }
        };
        for (TextField entry : saisieNomJoueur.getNomsJoueurs()) {
            entry.lengthProperty().addListener(tousLesChampsSontRemplis);
        }

        saisieNomJoueur.getStartGame().setOnAction(actionEvent -> {
            setListeDesNomsDeJoueurs();
        });

    }



}
