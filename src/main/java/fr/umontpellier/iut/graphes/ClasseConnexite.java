package fr.umontpellier.iut.graphes;

import java.util.ArrayList;

public class ClasseConnexite {
    private ArrayList<Integer> classe;

    public ClasseConnexite(){
        classe = new ArrayList<>();
    }

    public ClasseConnexite(ArrayList<Integer> classe){
        this.classe = classe;
    }

    public void ajouter(Integer sommet){
        classe.add(sommet);
    }

    public void enlever(Integer sommet){
        classe.remove(sommet);
    }

    public ArrayList<Integer> getClasse(){
        return classe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClasseConnexite that = (ClasseConnexite) o;
        for (Integer i : ((ClasseConnexite) o).classe){
            if (!classe.contains(i)){
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int res = 0;
        for (Integer i : classe){
            res+=i;
        }
        return res;
    }
}
